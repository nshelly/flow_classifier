#!/usr/bin/python
# flow_parser.py
#
# Author: Nick Shelly, Spring 2013
# Description:
#     - Randomly generates packet
#     - Parses flow of a packet through Open vSwitch flow table.
#         or loading the graph if has already been created.
#           
#    usage: flow_parser.py [-h] [-v] [-i INPUT_PACKET] [-r INPUT_FLOW_FILE]
#
#    optional arguments:
#      -h, --help            show this help message and exit
#      -v, --verbose         increase output verbosity
#      -i INPUT_PACKET, --input_packet INPUT_PACKET
#      -r INPUT_FLOW_FILE, --input_flow_file INPUT_FLOW_FILE
#
#   Example:
#   $ python flow_parser.py -v -r flow_table.txt

import re
import argparse
from copy import deepcopy
from collections import defaultdict
from random import choice, randrange
import matplotlib.pyplot as plt

# Run-time options
DEBUG=0
TEST_MODE=1

# Number of times to test each rule (with randomly generated packets) 
NUM_RULE_PACKETS_TESTS=100
NUM_RANDOM_PACKETS=10

SET_REG_ZERO=0
HOP_CUTOFF = 20

DEFAULT_CHASSIS_NUM=1
DEFAULT_FLOW_FILE="f.txt"
FIGURE_PREFIX="chassis%d" % DEFAULT_CHASSIS_NUM

# Default packet fields
IN_PORTS_BITS = 32 

VLAN_VID_BITS = 12 
DEFAULT_DL_VLAN_VID = 0

NUM_REGISTERS = 8
REGISTER_BITS = 32
DEFAULT_REG_MASK = (1 << REGISTER_BITS) - 1

TUNNEL_ID_BITS = 64
DEFAULT_TUNNEL_ID_MASK=(1 << TUNNEL_ID_BITS) - 1

DL_ADDR_BITS=48
DEFAULT_DL_MASK="ff:ff:ff:ff:ff:ff"

# Default input packet
DEFAULT_DL_SRC = "00:01:02:03:00:01"
DEFAULT_DL_DST = "01:80:c2:00:00:06"
DEFAULT_TUNNEL_ID = 0
DEFAULT_REGISTERS = ",".join(["0"] * NUM_REGISTERS)

verbose=0

class Fields:

  def __init__(self, registers=None, register_masks=None,
              tunnel_id=None, tunnel_id_mask=DEFAULT_TUNNEL_ID_MASK,
              dl_src=None, dl_src_mask=DEFAULT_DL_MASK,
              dl_dst=None, dl_dst_mask=DEFAULT_DL_MASK, 
              dl_vlan_vid=None, in_port=None):

    # TODO: Move registers to Packet and Rule class
    self.registers = registers
    self.register_masks = register_masks

    # Network filters
    self.dl_src = dl_src
    self.dl_src_mask = dl_src_mask
    self.dl_dst = dl_dst
    self.dl_dst_mask = dl_dst_mask

    self.dl_vlan_vid = dl_vlan_vid
    self.in_port = in_port

    # 64-bit value, with mask
    self.tunnel_id = tunnel_id
    self.tunnel_id_mask = tunnel_id_mask

  def __repr__(self):
    output = ""
    for key in sorted(vars(self).iterkeys()):
      value = getattr(self, key)
      if value in [None, DEFAULT_DL_MASK, DEFAULT_TUNNEL_ID_MASK, \
                         DEFAULT_REG_MASK] or key == "register_masks":
        continue
      elif "registers" in key:
        if self.register_masks is not None:
          reg_output = "\n".join(["\t\tREG%d => 0x%08x (%d) / 0x%08x" %\
                                  (i+1, val, val, mask) for i, (val, mask) in\
                                  enumerate(zip(value, self.register_masks))\
                                  if val is not None])

        else:
          reg_output = "\n".join(["\t\tREG%d => 0x%08x (%d)" %\
                                  (i+1, val, val) for i, val in enumerate(value)])
        if reg_output:
          output += "\n%s" % reg_output
        continue
      elif "tunnel_id" in key:
        output += "\n\t\t%s => 0x%016x (%d)" % (key, value, value)
      
      else:
        output += "\n\t\t%s => %s" % (key, repr(value))
    return output

class Packet:

  def __init__(self, **fields):

    self.hops = 0
    self.previous_rules = []

#    # Set packet defaults for undefined fields.
#    if "tunnel_id" not in fields:
#      fields["tunnel_id"] = DEFAULT_TUNNEL_ID

    self.fields = Fields(**fields)
  
  def __repr__(self):
    output = "Packet: \n\tHops: %d\n\tFields: %s" % (self.hops, self.fields)
    return output
 
class Rule:
  
  def __init__(self, rule_id, priority=None, actions=None, **fields):
    self.rule_id = rule_id
    self.priority = priority
    self.actions = actions
    self.fields = Fields(**fields)

  def __repr__(self):
    output = "Rule %04d: \n\tPriority: %s\n\tFields: %s\n\tActions: \n%s\n" % \
              (self.rule_id % 10000, self.priority, self.fields, \
                "\n".join(["\t\t%s" % repr(a) for a in self.actions]))

    return output

def dl_mask(addr_str, net_str, mask_str):
  '''
    Returns TRUE if the L2 address meets the net bitmask.
  '''
  
  addr = [int(val, 16) for val in addr_str.split(":")]
  mask = [int(val, 16) for val in mask_str.split(":")]
  net = [int(val, 16) for val in net_str.split(":")]
  #if verbose:
  #  print "addr = ", addr_str
  #  print "net = ", net_str
  #  print "mask = ", mask_str
  for val, vnet, vmask in zip(addr, net, mask):
    #if verbose: print "Checking %02X & %02X == %02X" % (val, vmask, vnet),
    # REG1=1/3 would require the first two bits to be '01'
    if val & vmask != vnet:
      if verbose: print "addr=%s & mask=%s ==? net=%s bad!" % (addr_str, mask_str, net_str)
      return False
      
  return True
  
class FlowClassifier:

  def __init__(self, rules={}, chassis_num=1, common_registers=[], 
                     common_dl_addrs=[]):
    self.rules = rules
    self.chassis_num = chassis_num

    # Keep track of which rule got acted upon.
    self.counters = defaultdict(int) 
    self.packets_output = 0

    # Common registers for generating new packets.
    self.common_registers = common_registers
    self.common_in_ports = []
    self.common_dl_addrs = []
    self.common_dl_vlan_vids = []
    self.common_tunnel_ids = []

  def verify_rules_covered(self):
    rules_not_found = []
    for rule in self.rules:
      if rule not in self.counters:
        print "Rule not triggered: %04d" % (rule % 10000)
        rules_not_found.append(rule)
    return rules_not_found
    
  def process_packet(self, hop_dict, packets_in_flight):

    matched_rule = None
    while len(packets_in_flight) > 0:
      if verbose: print "Packets in flight: %d" % len(packets_in_flight)
      packet = packets_in_flight.pop()
      packet.hops += 1
      matched_rule = self.match(packet)
      if not matched_rule: 
        print "No rule matches packet: %s" % packet
        continue

      self.counters[matched_rule.rule_id] += 1
      packet.previous_rules.append(matched_rule)
      if packet.hops < HOP_CUTOFF:
        new_packets = self.act(matched_rule, packet)
        packets_in_flight += new_packets

        if verbose and new_packets:
          print "New Packets:"
          for p in new_packets:
            print p

        # If packet hit last hop, tally
        if 0 == len(new_packets):
          hop_dict[packet.hops] += 1

      else:
        print "Packet exceeded cutoff %d! %s" % (HOP_CUTOFF, packet)

      if DEBUG:
        ok = raw_input("ok?")

  def match(self, packet):
    '''
      Find rules that matches
      Returns rule of highest priority.    
    '''

    matched_rule = None
    for rule in self.rules.values():
      if verbose: print "Checking Rule %d: fields=%s" % (rule.rule_id % 10000, rule.fields)
      match = True
      if rule.fields.in_port and packet.fields.in_port != rule.fields.in_port:
        match = False
        continue

      if rule.fields.dl_vlan_vid and \
          packet.fields.dl_vlan_vid != rule.fields.dl_vlan_vid:
        match = False
        continue

      if rule.fields.dl_dst and \
        not dl_mask(packet.fields.dl_dst, rule.fields.dl_dst, 
                    rule.fields.dl_dst_mask):
        match = False
        continue

      if rule.fields.dl_src and \
        not dl_mask(packet.fields.dl_src, rule.fields.dl_src, 
                    rule.fields.dl_src_mask):
        match = False
        continue

      if rule.fields.tunnel_id and \
            rule.fields.tunnel_id_mask & packet.fields.tunnel_id \
            != rule.fields.tunnel_id:
        match = False
        continue

      if rule.fields.registers:
        for i, (rule_val, rule_mask) in enumerate(zip(rule.fields.registers, 
                                                      rule.fields.register_masks)):
          if rule_val is not None:
            if verbose:
              print "Checking REG%d: 0x%08x & 0x%08x ==? 0x%08x" % \
                  (i+1, packet.fields.registers[i], rule_mask, rule_val),
            if packet.fields.registers[i] & rule_mask != rule_val:
              match = False
              if verbose: print "Bad!"
              break
            else:
              if verbose: print "Ok"
        if not match:
          continue

      if verbose:
        if match:
          print "Rule %d matched! %s" % (rule.rule_id % 10000, rule)
          if matched_rule:
            print "Bump existing rule? %s" % matched_rule
      
      # TODO: Figure out what happens when two rules are of equal priority.
      # For now, accept last
      if match and \
        (not matched_rule or matched_rule.priority <= rule.priority):
        matched_rule = rule
        if verbose: print "Match with highest priority!\n"
      else:
        if verbose: print "No match of highest priority."
      # end for loop - matching

    return matched_rule
    
  def parse_var(self, packet, flow_str):
    re_load = re.compile (r"(?P<var>[A-z]+)(?P<index>\d+)?\[(?P<start>\d+):(?P<stop>\d+)\]")
    m = re_load.match(flow_str)
    if verbose: print "m groups = ", m.groups()
    var = m.group('var').lower()
    if var == "reg":
      n = int(m.group('index')) - 1
      value = packet.fields.registers[n]
    elif var == "tunnel":
      n = None
      value = packet.fields.tunnel_id
    else:
      raise Exception("Unknown register var, %s" % flow_str)
    start = int(m.group('start'))
    stop = int(m.group('stop'))

    return (value, var, n, start, stop)


  def act(self, rule, old_packet):
    '''
      Takes an input packet, which the rule will act upon.
      Returns a list of outgoing packets (empty if dropped).
    '''

    def zero_out(value, start, stop):
      ''' 
        Zero out old value at range before setting.
        For example, REG8[12:19] <- TUNNEL[32:33], would zero out 
        bits 12 through 19)
      '''
      for i in range(start, stop):
        value &= ~(1 << i)
      return value

    if verbose:
      print "Applying Rule to Packet", rule, old_packet
    output_packets = []
    packet = old_packet

    for a in rule.actions:
      if verbose:
        print "-" * 20, "\nApplying action = ", a
      name, action = a
      if name in ["RESUBMIT", "RESUBMIT_TABLE"]:
        # TODO: Check if this is the proper action for resubmit table
        if "RESUBMIT" == name:
          port = int(action.split()[1])
          packet.fields.in_port = port
        # Add packet to new packet list, and make a new copy.
        output_packets.append(packet)
        packet = old_packet

      elif "SET_VLAN_VID" == name:
        if verbose: print "Setting vlan_vid to %d" % int(action)
        packet.fields.dl_vlan_vid = int(action)
        
      elif "REGISTER_MOVE" == name:
        # RegisterMove: REG8[12:19] <- TUNNEL[32:39]
        # RegisterMove: TUNNEL[0:17] <- TUNNEL[40:57]
        output_str, input_str = action.split(" <- ")

        (in_value, in_type, in_index, in_start, in_stop) = \
                  self.parse_var(packet, input_str)
        
        (out_value, out_type, out_index, out_start, out_stop) = \
                  self.parse_var(packet, output_str)

        if out_stop - out_start != in_stop - in_start:
          raise "Invalid RegisterLoad!"

        # Get input value at offset.
        # For example, if the tunnel's bits 32 and 33 were both 1,
        # TUNNEL[32:33], would return 0b11 (or 3).
        new_value = in_value >> in_start
        new_value &= (1 << (in_stop - in_start + 1)) - 1
        
        if verbose:
          print "Before: out_value = %s" % bin(out_value) 
          print "Before: new_value = %s" % bin(new_value) 

        out_value = zero_out(out_value, out_start, out_stop)

        # Set the value
        out_value |= new_value << out_start
        # Make sure we don't overflow at the end of the op (use mask).
        if out_type == "reg":
          packet.fields.registers[out_index] = out_value & DEFAULT_REG_MASK
          if verbose:
            print "After reg = %s (%d)" % (bin(packet.fields.registers[out_index]),\
                                         packet.fields.registers[out_index])

        elif out_type == "tunnel":
          packet.fields.tunnel = out_value & DEFAULT_TUNNEL_ID_MASK
          if verbose:
            print "After tunnel = %s (%d)" % (bin(packet.fields.tunnel),\
                                              packet.fields.tunnel)
        else:
          raise Exception("Unknown register, %s" % output_str)

      elif "REGISTER_LOAD" in name:
        output_str, value_str = action.split(" <- ")
        new_value = int(value_str)

        (out_value, out_type, out_index, out_start, out_stop) = \
                      self.parse_var(packet, output_str)

        if new_value >= 2 << (out_stop - out_start):
          raise Exception("New value too large for registers!")

        if verbose:
          print "Before: out_value = %s (%d)" % (bin(out_value), out_value)
          print "Before: new_value = %s (%d)" % (bin(new_value), new_value)

        out_value = zero_out(out_value, out_start, out_stop)

        # Set the value
        out_value |= new_value << out_start
        # Make sure we don't overflow at the end of the op (use mask).
        if out_type == "reg":
          packet.fields.registers[out_index] = out_value & DEFAULT_REG_MASK
          if verbose:
            print "After reg = %s (%d)" % (bin(packet.fields.registers[out_index]), packet.fields.registers[out_index]) 

        elif out_type == "tunnel":
          packet.fields.tunnel = out_value & DEFAULT_TUNNEL_ID_MASK
          if verbose:
            print "After tunnel = %s (%d)" % (bin(packet.fields.tunnel), packet.fields.tunnel)
        else:
          raise Exception("Unknown register, %s" % output_str)

      elif name in ['NOTE', 'LEARN', 'BUNDLE_LOAD']:
        if verbose: print a
        pass
    
      elif name in ["OUTPUT", "OUTPUT_REG"]:
        # For now don't resubmi
        if verbose: print "output packet!"
        self.packets_output += 1
        pass

      else:
        raise Exception("Error: Unknown action: %s" % name)
    return output_packets
      
  def read_file(self, filename):
    '''
      Read in file and create flow table.
    '''

    # Print header (4 lines)
    f = open(filename)
    chassis = f.read().split("Chassis")[DEFAULT_CHASSIS_NUM]
    rules = chassis[3:].split("----")
    common_registers = [[] for i in range(NUM_REGISTERS)]
    common_dl_addrs = []
    common_dl_vlan_vids = []
    common_tunnel_ids = []
    common_in_ports = []
    for rule_text in rules:
      re_filter = re.compile("\nnx_expr:.*\n(.*)\n")
      m  = re_filter.search(rule_text)
      if m is None:
        break
      else:
        rule_line = m.groups()[0]
      rule_line = rule_line.split(", ")

      # Populate with rule filters.
      flags = {}
      reg = [None] * NUM_REGISTERS
      reg_masks = [DEFAULT_REG_MASK] * NUM_REGISTERS
      for match in rule_line:
        name, detail = match.split("=")
        name = name.lower()

        # Skip wildcards for now.
        if "wildcards" == name:
          continue

        detail = detail.split("/")
        # Convert to int (or long) for masking.
        if name in ["priority", "tunnel_id", "dl_vlan_vid"] or "reg" in name:
          detail = map(int, detail)

        value = detail[0]
        mask = detail[1] if len(detail) > 1 else None           
        if "reg" in name:
          r = int(name[3]) - 1
          reg[r], reg_masks[r] = value, mask
          common_registers[r].append(value)
        else:
          flags[name] = value

        if name in ["tunnel_id", "dl_src", "dl_dst"] and mask:
          flags["%s_mask" % name] = mask
    
        if name in ["in_port"]:
          common_in_ports.append(value)

        if name in ["dl_src", "dl_dst"]:
          common_dl_addrs.append(value)

        if name == ["tunnel_id"]:
          common_tunnel_ids.append(value)

        if name == ["dl_vlan_vid"]:
          common_dl_vlan_vids.append(value)

        # end for loop - match

      flags["registers"] = reg
      flags["register_masks"] = reg_masks

      # Continue to rule's actions
      re_action = re.compile(".*\nactions: \[(.*)\].*", re.DOTALL)
      action_text = re_action.match(rule_text).group(1)
      action_groups = action_text.strip().split("message_type: 22")[:-1]
      #print "action_groups = ", action_groups
      actions = []
      for atext in action_groups: 
        #print "atext = ", atext
        re_atype = re.compile(".*type: (?P<type>[^,]*),.*Extensions: \{"
                              "\n(?P<extensions>.*)\}.*", re.DOTALL)
        m = re_atype.match(atext)
        atype = m.group('type')
        aext = m.group('extensions').split(":", 1)[1].strip().split(",\n")
      
        if atype in ["LEARN", "BUNDLE_LOAD"]:
          # Skip to next action
          action = None

        elif atype == "NOTE":
          action = aext[0]

        elif atype in ["REGISTER_MOVE", "REGISTER_LOAD", "RESUBMIT_TABLE", 
                                                         "RESUBMIT"]:
          action = aext[0].split(": ", 1)[1]

        elif atype == "SET_VLAN_VID":
          action = int(aext[2].split(": ")[1])

        elif atype == "OUTPUT":
          action = int(aext[0].split(": ")[1])

        elif atype == "OUTPUT_REG":
          action = aext[0].split(" = ")[1].split(",")[0]

        elif atype == "STRIP_VLAN":
          atype = "SET_VLAN_VID"
          action = "0"
        else:
          raise Exception("Unknown type: %s" % atype)

        actions.append((atype, action))
        # end while loop - actions

      flags["actions"] = actions

      re_id = re.compile(".*\nid: (?P<id>\d*).*", re.DOTALL)
      rule_id = int(re_id.match(rule_text).group('id'))
      flags["rule_id"] = rule_id
  
      new_rule = Rule(**flags)
      if verbose:
        print new_rule
      self.rules[new_rule.rule_id] = new_rule

      #ok = raw_input("ok?")
    print "Read %d rules" % (len(self.rules))
    self.common_registers = [set(r) for r in common_registers]
    self.common_dl_vlan_vids = set(common_dl_vlan_vids)
    self.common_tunnel_ids = set(common_tunnel_ids)
    self.common_in_ports = set(common_in_ports)
      
def generate_packet(classifier, predefined_fields={}):

  packet = Packet(**predefined_fields)
  for field in packet.fields.__dict__.keys():

    # Skip pre-defined values.
    if field in predefined_fields and predefined_fields[field] is not None:
      continue

    elif "mask" in field:
      continue

    elif "tunnel_id" == field:
      # Choose between 0 and a ranomd number, 50% of the time
      packet.fields.tunnel_id = choice(list(classifier.common_tunnel_ids) +
                                       [randrange(0, 1 << TUNNEL_ID_BITS)])

    elif "in_port" == field:
      packet.fields.in_port = choice(list(classifier.common_in_ports) +
                                     [randrange(1 << IN_PORTS_BITS)])

    elif "dl_vlan_vid" == field:
      packet.fields.dl_vlan_vid = choice(list(classifier.common_dl_vlan_vids) +
                                         [randrange(1 << VLAN_VID_BITS)])

    elif field in ["dl_src", "dl_dst"]:
      setattr(packet.fields, field, 
              choice(list(classifier.common_dl_addrs) + 
                     [":".join("%02x" % randrange(1 << 8) 
                      for i in range(DL_ADDR_BITS/8))]))

    elif "registers" == field:
      # Use either common registers or randomly generated register.
      registers = [choice(list(classifier.common_registers[i]) +
                           [randrange(1 << REGISTER_BITS)]) \
                            for i in range(NUM_REGISTERS)]
      if SET_REG_ZERO:
        registers[0] = 0
      packet.fields.registers = registers
       
    else:
      raise Exception("Unknown field: %s" % field)

    # end for loop fields

  # Set undefined registers
  for r in range(NUM_REGISTERS):
    if packet.fields.registers[r] is None: 
      packet.fields.registers[r] = choice(list(classifier.common_registers[r]) +
                                          [randrange(1 << REGISTER_BITS)])

  return packet

def main():
  
  global verbose, results_dir, flow_file, packet

  parser = argparse.ArgumentParser()
  parser.add_argument("-v", "--verbose", default=False,
                      action="store_true", dest="verbose",
                      help="increase output verbosity")

  parser.add_argument("-f", "--input_flow_file", default=DEFAULT_FLOW_FILE)
  parser.add_argument("-i", "--input_packet", type=int, required=False)

  # Packet input values
  parser.add_argument("-t", "--tunnel_id", type=int, default=DEFAULT_TUNNEL_ID)
  parser.add_argument("-l", "--dl_vlan_vid", type=int, 
                      default=DEFAULT_DL_VLAN_VID)
  parser.add_argument("-s", "--dl_src", default=DEFAULT_DL_SRC)
  parser.add_argument("-d", "--dl_dst", default=DEFAULT_DL_DST)
  parser.add_argument("-r", "--registers", default=DEFAULT_REGISTERS,
                      help="Register values (comma-separated)")

  args = parser.parse_args()

  verbose = args.verbose
  flow_file = args.input_flow_file
  #packet = args.input_packet

  classifier = FlowClassifier()
  classifier.read_file(flow_file)
  
  if TEST_MODE:
    hop_dict = defaultdict(int)
 
    # Test all edges cases with rules
    for i in range(NUM_RULE_PACKETS_TESTS):
      for rule in classifier.rules.values():
        packet = generate_packet(classifier, predefined_fields=deepcopy(rule.fields.__dict__))
        if verbose:
          print "Based on Rule %d %s, \nPacket=%s" % \
              (rule.rule_id % 10000, rule.fields, packet)
        classifier.process_packet(hop_dict, [packet])

    for i in range(NUM_RANDOM_PACKETS):
      packet = generate_packet(classifier)  
      if verbose: print "Generated Packet: ", packet
      classifier.process_packet(hop_dict, [packet])
    rules_not_used = classifier.verify_rules_covered()
    if not rules_not_used:
      print "All rules used!"
    print "Processed %d rule-based packets and %d fully random packets." % \
          (NUM_RULE_PACKETS_TESTS * len(classifier.rules), NUM_RANDOM_PACKETS)

    # Plot rules distribution
    print "Hops: %s" % repr(hop_dict)
    plt.figure()
    plt.bar(hop_dict.keys(), hop_dict.values(), align="center")
    plt.xticks(hop_dict.keys())
    plt.title("# of Hops")
    plt.draw()
    plt.savefig("hops_dist_%s.png" % FIGURE_PREFIX)

    print "Counter - Top 10 Rules:"
    print "-" * 10
    import operator
    for key, value in sorted(classifier.counters.iteritems(), 
                             key=operator.itemgetter(1), reverse=True)[:10]:
      print "Rule %04d: %d" % (key % 10000, value)

    counter_list = sorted([val for val in classifier.counters.values()])
    plt.figure()
    plt.scatter([r for r in range(len(counter_list))], counter_list)
    plt.title("Rules counter")
    plt.xlabel("Rule")
    plt.xlim(xmin=0)
    plt.ylabel("Number of occurences")
    plt.ylim(ymin=0)
    plt.draw()
    plt.savefig("rules_counter_%s.png" % FIGURE_PREFIX)

    # Plot rule counter distribution
    rules_counter_dist = defaultdict(int)
    for val in classifier.counters.values():
      rules_counter_dist[val] += 1
    print "Matched distribution: %s" % rules_counter_dist
    plt.figure()
    plt.bar(rules_counter_dist.keys(), rules_counter_dist.values(), 
            align="center")
    plt.xticks(rules_counter_dist.keys())
    plt.title("Rules counter (distribution)")
    plt.xlabel("rule occurrence")
    plt.ylabel("Number of rules")
    plt.xscale('log')
    plt.draw()
    plt.savefig("rules_dist_%s.png" % FIGURE_PREFIX)

  else:
    # Use user-input packet
    fields = {}
    fields["dl_src"] = args.dl_src
    fields["dl_dst"] = args.dl_dst
    fields["tunnel_id"] = args.tunnel_id
    fields["dl_vlan_vid"] = args.dl_vlan_vid
    initial_reg = [None] * NUM_REGISTERS
    for i, r in enumerate(args.registers.split(",")):
      initial_reg[i] = int(r)
    fields["registers"] = initial_reg
    packet = Packet(**fields)
    classifier.process_packet([packet])

if __name__ == "__main__":
  main()
