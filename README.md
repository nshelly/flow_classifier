OpenFlow Flow Classifer
===========

1. Checkout the Flow Classifer code, and initialize the submodules (Hassel-C).

		git clone git@bitbucket.org:nshelly/flow_classifier.git
		git submodule init
		git submodule update

2. Then, compile the Hassel-C library using the install script.

		cd hassel-public/hsa-python/
		./setup.sh
		
3. To run the flow classifier on a flow table, include the table as a text file as the last argument.  For example:

		$ python flow_spacer.py -h
		Logging to python_1008_0329.16.log
		Usage: flow_spacer.py [options]
		
		Options:
		  -h, --help            show this help message and exit
		  -r, --redo            redo flow parsing, even if file exists
		  -d RESULTS_DIR, --results_dir=RESULTS_DIR
		                        directory to store results
		  -q, --quiet           don't print status messages to stdout
		  -m MIN_RULES, --min_rules=MIN_RULES
		                        min number of rules
		  -x MAX_RULES, --max_rules=MAX_RULES
		                        max number of rules
		  -c HOP_CUTOFF, --hop_cutoff=HOP_CUTOFF
		                        cutoff before stopping routing
                        	
		$ python flow_spacer.py -x 60 -c 12 dat_backups/topo-00/tables.txt
		
4. There are some example flow tables in the `dat_backups` directory. For example, to run a parser that includes at least 60 flows at a minimum with a hop-cutoff of 12, run this command. The results should be output to a `results` directory:

		$ cat results/*
		FE, Rules, Flows, Subset_Rules_Excluded, Num_Comparisons
		00-32-bit_0912d470-98f5-46e0-a55e-27cfa2cee11f, 59, 16131, 5410, 24766
		FE, Rules, Flows, Subset_Rules_Excluded, Num_Comparisons
		00-32-bit_182a781a-fb1b-4e34-833c-5844f8a1e278, 26, 364, 104, 746
		FE, Rules, Flows, Subset_Rules_Excluded, Num_Comparisons
		00-32-bit_50188a05-1d85-4afa-98c7-f19936dcc16f, 59, 16389, 5512, 25125
		FE, Rules, Flows, Subset_Rules_Excluded, Num_Comparisons
		80-32-bit_f12e6e8d-d46d-4369-9319-766c9432a322, 251, 2742, 2808, 81497
		
An example log file at the end of processing the flows shows:

		DEBUG:root:Converting to list
		DEBUG:root:Rule dump:
		[0] -  (-1)
		[1] -  (-1)
		[2] -  (-1)
		[3] -  (-1)
		[4] -  (-1)
		[5] -  (-1)
		[6] -  (-1)
		[7] -  (-1)
		[8] -  (-1)
		[9, 45, 46] -  (-1)
		[9, 45, 56, 58] -  (-1)
		[9, 45, 56] -  (-1)
		[9, 45, 58] -  (-1)
		[9, 45] -  (-1)
		[9, 58] -  (-1)
		…
		[57, 37, 33, 45] -  (-1)
		[57, 37, 33, 58] -  (-1)
		[57, 37, 33] -  (-1)
		[57, 37, 45, 46] -  (-1)
		[57, 37, 45, 56, 58] -  (-1)
		[57, 37, 45, 56] -  (-1)
		[57, 37, 45, 58] -  (-1)
		[57, 37, 45] -  (-1)
		[57, 37, 58] -  (-1)
		[57, 37] -  (-1)
		[57, 47] -  (-1)
		[57, 52] -  (-1)
		[57, 58] -  (-1)
		[57] -  (-1)
		[58] -  (-1)
		[] -  (-1)
		16387 rules, 5512 subset, 25125 comps
		DEBUG:root:Finished matching rules.
		DEBUG:root:Converting to list
		DEBUG:root:Received 16389 new results