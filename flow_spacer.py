#!/usr/bin/python
# flow_spacer.py
#
# Author: Nick Shelly, Spring 2013
# Description:
#     - Identifies flow headerspaces based on rules provided in the Open vSwitch flow table,
#           
#    usage: flow_spacer.py [-h] [-v] [-i INPUT_PACKET] [-r INPUT_FLOW_FILE]
#
#    optional arguments:
#      -h, --help            show this help message and exit
#      -v, --verbose         increase output verbosity
#      -i INPUT_PACKET, --input_packet INPUT_PACKET
#      -r INPUT_FLOW_FILE, --input_flow_file INPUT_FLOW_FILE
#
#   Example:
#   $ python flow_spacer.py -v -r flow_table.txt

import os
import re
from logging import debug, info, basicConfig, DEBUG
from collections import defaultdict
import cPickle as pickle

import sys
sys.path.append(os.path.join(os.path.dirname(__file__), \
                "hassel-public/hsa-python"))
sys.path.append(os.path.join(os.path.dirname(__file__), \
                "hassel-public/hsa-python/utils"))

from utils.helper import *
from headerspace import hs, tf
from utils.wildcard import *
from utils.wildcard_utils import get_header_field,set_header_field
from utils.wildcard_utils import wc_header_to_parsed_string

# Run-time options
TEST_MODE = 1
DEFAULT_MIN_RULES = 5
DEFAULT_MAX_RULES = 1000
DEFAULT_HOP_CUTOFF = 12

# Number of times to test each rule (with randomly generated packets) 
NUM_RULEBASED_PACKETS_TESTS = 100
NUM_RANDOM_PACKETS = 10

SET_REG_ZERO = 0

DEFAULT_CHASSIS_NUM=1
DEFAULT_FLOW_FILE="f.txt"
FIGURE_PREFIX="chassis%d" % DEFAULT_CHASSIS_NUM
DEFAULT_TABLE_NO = 0
DEFAULT_RESULTS_DIR="results"

# Default packet fields
IN_PORTS_BITS = 32 

VLAN_VID_BITS = 16 
DEFAULT_DL_VLAN_VID = 0

DEFAULT_NUM_REGISTERS = 8
REGISTER_BITS = 32
DEFAULT_REG_MASK = (1 << REGISTER_BITS) - 1

TUNNEL_BITS = 64
DEFAULT_TUNNEL_MASK=(1 << TUNNEL_BITS) - 1

DL_ADDR_BITS=48
NW_ADDR_BITS=32
DEFAULT_DL_MASK="ff:ff:ff:ff:ff:ff"

METADATA_BITS=32

# Default input packet
#DEFAULT_DL_SRC = "00:01:02:03:00:01"
#DEFAULT_DL_DST = "01:80:c2:00:00:06"
#DEFAULT_tunnel = 0

verbose=True

class Wildcards:
    PRIORITY    = 1 << 8
    IN_PORT     = 1 << 0
    DL_VLAN_VID = 1 << 1
    DL_VLAN_PCP = 1 << 9
    DL_SRC      = 1 << 2
    DL_DST      = 1 << 3
    DL_TYPE     = 1 << 4
    NW_TOS      = 1 << 10
    NW_PROTO    = 1 << 5
    NW_SRC      = 1 << 11
    NW_DST      = 1 << 12
    TP_SRC      = 1 << 6
    TP_DST      = 1 << 7
    EXPR_ALL    = (1 << 13) - 1

class FlowActionTypes:
    # identifiers defined in NIB and referenced in FlowActionPB.type
    DROP          = 1
    OUTPUT        = 2
    SET_VLAN_VID  = 3
    SET_VLAN_PCP  = 4
    STRIP_VLAN    = 5
    SET_DL_SRC    = 6
    SET_DL_DST    = 7
    SET_NW_SRC    = 8
    SET_NW_DST    = 9
    SET_NW_TOS    = 10
    SET_TP_SRC    = 11
    SET_TP_DST    = 12
    ENQUEUE       = 13
    RESUBMIT      = 14
    REGISTER_MOVE = 15
    REGISTER_LOAD = 16
    SAMPLE        = 17
    # NiciraActionTypes
    SET_QUEUE           = 0x1000
    POP_QUEUE           = 0x2000
    NOTE                = 0x3000
    MULTIPATH           = 0x4000
    AUTOPATH            = 0x5000
    RESUBMIT_TABLE      = 0x6000
    LEARN               = 0x7000
    BUNDLE_LOAD         = 0x8000
    BUNDLE_OUTPUT       = 0x9000
    OUTPUT_REG          = 0x9001
    DECREMENT_TTL       = 0x9002
    OUTPUT_CONTROLLER   = 0x9003
    DECREMENT_TTL_CONTROLLER = 0x9004
    STACK_PUSH          = 0x9005
    STACK_POP           = 0x9006

class OfpActionTypes:
    # action types defined in ofp_action_type
    OFPAT_OUTPUT       = 0
    OFPAT_SET_VLAN_VID = 1
    OFPAT_SET_VLAN_PCP = 2
    OFPAT_STRIP_VLAN   = 3
    OFPAT_SET_DL_SRC   = 4
    OFPAT_SET_DL_DST   = 5
    OFPAT_SET_NW_SRC   = 6
    OFPAT_SET_NW_DST   = 7
    OFPAT_SET_NW_TOS   = 8
    OFPAT_SET_TP_SRC   = 9
    OFPAT_SET_TP_DST   = 10
    OFPAT_ENQUEUE      = 11
    OFPAT_VENDOR       = 0xffff

NX_VENDOR_ID = 0x00002320

class NxActionSubtypes:
    # action subtypes defined in nx_action_subtype
    NXAST_RESUBMIT     = 1
    NXAST_SET_TUNNEL   = 2
    NXAST_SET_QUEUE    = 4
    NXAST_POP_QUEUE    = 5
    NXAST_REG_MOVE     = 6
    NXAST_REG_LOAD     = 7
    NXAST_NOTE         = 8
    NXAST_SET_TUNNEL64 = 9
    NXAST_MULTIPATH    = 10
    NXAST_AUTOPATH     = 11
    NXAST_BUNDLE       = 12
    NXAST_BUNDLE_LOAD  = 13
    NXAST_RESUBMIT_TABLE = 14
    NXAST_OUTPUT_REG   = 15
    NXAST_LEARN        = 16
    NXAST_EXIT         = 17
    NXAST_DEC_TTL      = 18
    NXAST_FIN_TIMEOUT  = 19
    NXAST_CONTROLLER   = 20
    NXAST_DEC_TTL_CNT_IDS = 21

FlowActionTypeStrings = {
    FlowActionTypes.DROP          : "DROP",
    FlowActionTypes.OUTPUT        : "OUTPUT",
    FlowActionTypes.SET_VLAN_VID  : "SET_VLAN_VID",
    FlowActionTypes.SET_VLAN_PCP  : "SET_VLAN_PCP",
    FlowActionTypes.STRIP_VLAN    : "STRIP_VLAN",
    FlowActionTypes.SET_DL_SRC    : "SET_DL_SRC",
    FlowActionTypes.SET_DL_DST    : "SET_DL_DST",
    FlowActionTypes.SET_NW_SRC    : "SET_NW_SRC",
    FlowActionTypes.SET_NW_DST    : "SET_NW_DST",
    FlowActionTypes.SET_NW_TOS    : "SET_NW_TOS",
    FlowActionTypes.SET_TP_SRC    : "SET_TP_SRC",
    FlowActionTypes.SET_TP_DST    : "SET_TP_DST",
    FlowActionTypes.ENQUEUE       : "ENQUEUE",
    FlowActionTypes.RESUBMIT      : "RESUBMIT",
    FlowActionTypes.REGISTER_MOVE : "REGISTER_MOVE",
    FlowActionTypes.REGISTER_LOAD : "REGISTER_LOAD",
    FlowActionTypes.SET_QUEUE     : "SET_QUEUE",
    FlowActionTypes.POP_QUEUE     : "POP_QUEUE",
    FlowActionTypes.NOTE          : "NOTE",
    FlowActionTypes.MULTIPATH     : "MULTIPATH",
    FlowActionTypes.AUTOPATH      : "AUTOPATH",
    FlowActionTypes.RESUBMIT_TABLE: "RESUBMIT_TABLE",
    FlowActionTypes.LEARN         : "LEARN",
    FlowActionTypes.OUTPUT_REG    : "OUTPUT_REG",
    FlowActionTypes.BUNDLE_LOAD   : "BUNDLE_LOAD",
    FlowActionTypes.BUNDLE_OUTPUT : "BUNDLE_OUTPUT",
    FlowActionTypes.OUTPUT_REG    : "OUTPUT_REG",
    FlowActionTypes.DECREMENT_TTL : "DECREMENT_TTL",
    FlowActionTypes.OUTPUT_CONTROLLER : "OUTPUT_CONTROLLER",
    FlowActionTypes.DECREMENT_TTL_CONTROLLER : "DECREMENT_TTL_CONTROLLER",
    FlowActionTypes.STACK_PUSH    : "STACK_PUSH",
    FlowActionTypes.STACK_POP     : "STACK_POP",
    FlowActionTypes.SAMPLE        : "SAMPLE",
}

class NoteTypes:
    # in network byte order
    GENERATE_ARP_REPLY_NOTE      = 0
    UPDATE_ARP_CACHE_NOTE        = 0x100
    RESOLVE_ARP_NOTE             = 0x200
    ZERO_TTL_NOTE                = 0x300
    PROCESS_NVP_ARP_REQUEST_NOTE = 0x400
    LOCAL_DELIVERY_NOTE          = 0x500
    GRATUITOUS_ARP_NOTE          = 0x600
    GENERATE_ICMP_NOTE           = 0x700

    ANNOTATION_NOTE              = 0x0080
    LOGICAL_PORT_RELATION_NOTE   = 0x0180
    LOGICAL_PORT_PATCH_ATTACHMENT_RELATION_NOTE   = 0x0280

def mask(bit):
    assert bit <= 64, "unexpected bit: %d" % bit
    if bit == 64:
        return 0xffffffffffffffff
    else:
        return (1 << bit) - 1

def is_clr(wc, mask):
    assert (wc & Wildcards.EXPR_ALL) == wc, "unexpected wildcard: 0x%x" % wc
    return (wc & mask) == 0

class FlowClassifier(object):

    def __init__(self, num_regs=DEFAULT_NUM_REGISTERS, \
                     chassis_num=1, table_id=0, table_no=0, engine_id="",
                     TEST_FE=None, hop_cutoff=DEFAULT_HOP_CUTOFF):
        debug("Initializing flow classifier, fe=%s" % engine_id)
        
        self.chassis_num = chassis_num

        # Keep track of which rule got acted upon.
        self.counters = defaultdict(int) 

        self.engine_id = engine_id
        self.table_no = table_no
        self.table_id = table_id
        self.num_regs = num_regs

        self.hs_fields = self.HS_FIELDS(num_regs=num_regs)
        self.hs_format = self.HS_FORMAT(num_regs=num_regs)

        # Physical packet fields (e.g. DL_SRC, SL_DST, IN_PORT, etc)
        self.pkt_phys_fields = self.PKT_PHYS_FIELDS()

        # Common registers for generating new packets.
        self.common = {}
        for f in self.hs_fields:
            self.common[f] = set()
        self.common["tunnel"] = set()

        ## Entry points into the HSA transfer function:
        ## Rule Index -> [List of preliminary actions]
        #self.entry_points = {0: []}
        #self.curr_entry_point = 1

        # Tuple of "pre-actions" and subsequent actions

        self.tf = tf.TF(self.hs_format["length"], table_no=table_no,
                        hop_cutoff=hop_cutoff)
        self.tf.hs_fields = self.hs_fields
        self.tf.pkt_phys_fields = self.pkt_phys_fields
        self.tf.hs_format = self.hs_format

        self.TEST_FE = TEST_FE

        self.hs_to_output = []

        def verify_rules_covered(self):
            rules_not_found = []
            for rule in self.rules:
                if rule not in self.counters:
                    debug("Rule not triggered: %04d" % (rule % 10000))
                    rules_not_found.append(rule)
            return rules_not_found

    def __str__(self):
        debug("Printing HSA classifier")
        return "\n".join(self.tf.to_string())
      
    def get_field(self, arr, field, value_as_int=False):
        return get_header_field(self.hs_format, arr, field, value_as_int=True)

    @staticmethod
    def PKT_PHYS_FIELDS():
        return ["in_port","dl_vlan_vid","dl_src","dl_dst","dl_type", \
                 "nw_src", "nw_dst", "nw_proto", "tunnel_bh", "tunnel_th", \
                 "metadata", "queue", "arp_sha", "tp_src", "tp_dst"]

    @staticmethod
    def HS_FIELDS(num_regs=DEFAULT_NUM_REGISTERS):
        pkt_fields = FlowClassifier.PKT_PHYS_FIELDS()
        registers = ["reg%d" % (r+1) for r in range(num_regs)]
        return pkt_fields + registers

    @staticmethod
    def HS_FORMAT(num_regs=DEFAULT_NUM_REGISTERS):
        format = {}
        format["in_port_len"] = IN_PORTS_BITS/8
        format["dl_vlan_vid_len"] = VLAN_VID_BITS/8 
        format["dl_src_len"] = DL_ADDR_BITS/8
        format["dl_dst_len"] = DL_ADDR_BITS/8 
        format["dl_type_len"] = 4
        format["tunnel_th_len"] = TUNNEL_BITS/8/2
        format["tunnel_bh_len"] = TUNNEL_BITS/8/2
        format["metadata_len"] = METADATA_BITS/8
        format["queue_len"] = 4

        format["nw_src_len"] = NW_ADDR_BITS/8
        format["nw_dst_len"] = NW_ADDR_BITS/8
        format["nw_proto_len"] = 1

        format["arp_sha_len"] = DL_ADDR_BITS/8
        format["arp_tha_len"] = DL_ADDR_BITS/8
        format["tp_src_len"] = 4
        format["tp_dst_len"] = 4

        fields = [f[:-4] for f in sorted(format.keys())]

        ofs = 0
        for f in fields:
            format["%s_pos" % f] = ofs
            ofs += format["%s_len" % f]

        for r in range(num_regs):
          format["reg%d_len" % (r+1)] = REGISTER_BITS/8
          format["reg%d_pos" % (r+1)] = ofs
          ofs += REGISTER_BITS/8

        format["length"] = ofs
        return format

    def parse_var(self, flow_str):
        re_load = re.compile(r"(?P<field>\w+)?\[(?P<start>\d+):(?P<stop>\d+)\]")
        m = re_load.match(flow_str)
        # debug("flow_str = %s, m groups = %s" % (flow_str, m.groups()))
        field = m.group('field').lower()

        start = int(m.group('start'))
        stop = int(m.group('stop')) + 1

        return (field, start, stop)
      
    def add_entry_action(self, rule_actions):
        
        if self.curr_entry_point not in self.entry_points:
            self.entry_points[self.curr_entry_points] = []
        self.entry_points[self.curr_entry_point].append(action)

    def clear_non_matches(self, m):

        debug('matching %s' % m)

        if "in_port" in m and not is_clr(m['wildcards'], Wildcards.IN_PORT):
            m.pop("in_port")
            debug("deleting in_port")

        if "dl_vlan_vid" in m:
            m["dl_vlan_vid"] = m["dl_vlan_vid"] & 0xfff 

        if "dl_vlan_vid" in m and not is_clr(m['wildcards'], Wildcards.DL_VLAN_VID):
            m.pop("dl_vlan_vid")
            debug("deleting dl_vlan_vid")

        # dl_vlan_pcp
        if "dl_src" in m and not is_clr(m['wildcards'], Wildcards.DL_SRC):
            m.pop("dl_src")
            debug("deleting dl_src")

        if "dl_dst" in m and not (is_clr(m['wildcards'], Wildcards.DL_DST) or
                                  m["dl_dst_mask"] != 0xffffffffffff):
            m.pop("dl_dst")
            debug("deleting dl_dst")

        if "dl_type" in m and not is_clr(m['wildcards'], Wildcards.DL_TYPE):
            m.pop("dl_type")
            debug("deleting dl_type")

        # nw_tos
        if "nw_proto" in m and not is_clr(m['wildcards'], Wildcards.NW_PROTO):
            m.pop("nw_proto")
            debug("deleting nw_proto")

        if "nw_src" in m and not (is_clr(m['wildcards'], Wildcards.NW_SRC) or
                                        (m['nw_src'] != '' and m['nw_src'] != 0x0 
                                         and m['nw_src_mask'] != 0x0)):
            m.pop("nw_src")
            debug("deleting nw_src")

        if "nw_dst" in m and not (is_clr(m['wildcards'], Wildcards.NW_DST) or
                                        (m['nw_dst'] != '' and m['nw_dst'] != 0x0 
                                         and m['nw_dst_mask'] != 0x0)):
            m.pop("nw_dst")
            debug("deleting nw_dst")

        if "arp_sha" in m and m["arp_sha"] == 0x0:
            m.pop("arp_sha")
            debug("deleting arp_sha")

        if "arp_tha" in m and m["arp_tha"] == 0x0:
            m.pop("arp_tha")
            debug("deleting arp_tha")

        if "tp_src" in m and not is_clr(m['wildcards'], Wildcards.TP_SRC):
            m.pop("tp_src")
            debug("deleting tp_src")

        if "tp_dst" in m and not is_clr(m['wildcards'], Wildcards.TP_DST):
            m.pop("tp_dst")
            debug("deleting tp_dst")

        return m

    def parse_rules(self, rules, filename=None, results_dir=None, load=True, \
                          save=True, redo=False, min_rules=DEFAULT_MIN_RULES,
                          max_rules=DEFAULT_MAX_RULES):

        def normalize_tunnel(start, stop, field):
            assert (field == "tunnel")
            if stop <= 32:
                # First half of tunnel ID
                field = "tunnel_bh"
            else: 
                # Second half of tunnel ID
                if start < 32:
                    raise Exception("Unable to add tunnel move (> 32 bits)")
                else:
                    start -= 32
                    stop -= 32
                    field = "tunnel_th"
            #debug("Returning %s" % field)
            return (start, stop, field)

        if filename:
            self.default_filename = "%s.dat" % filename
        else:
            self.default_filename = "hsa_tables_%s.dat" % self.engine_id

        #if not redo and os.path.exists(self.default_filename):
        #    self.load_from_file(self.default_filename)
        #    return

        if not self.TEST_FE or self.engine_id in self.TEST_FE:
            info("Parsing TF rules for fe=%s" % self.engine_id)
        else:
            info("Skipping parsing TF rules for fe=%s" % self.engine_id)
            return

        debug("Parsing %d rules for Table %d" % (len(rules), self.tf.table_no))

        debug("Min: %d, Max: %d", min_rules, max_rules)
        if len(rules) < min_rules or len(rules) > max_rules:
            debug("Skipping")
            return

        for idx, rule_text in enumerate(rules):
            debug("rule_text = %s" % rule_text)
            re_filter = re.compile("\snx_expr:.*\n(.*)\s")
            m  = re_filter.search(rule_text)
            if m is None:
                break
            else:
                rule_line = m.groups()[0].strip().split(", ")
            debug("rule_line: %s" % rule_line)

            # Populate with rule filters.
            flags = {}
            reg = [None] * self.num_regs
            reg_masks = [DEFAULT_REG_MASK] * self.num_regs

            wildcard_value = 0xfff
            priority = 0
            match_fields = {}
            for filter_str in rule_line:
                name, detail = filter_str.split("=")
                name = name.lower()

                detail = detail.split("/")
                debug("name=%s, detail=%s" % (name, detail))

                # Convert to int (or long) for masking.
                if name in ["priority", "tunnel_id", "dl_vlan_vid",
                            "in_port","metadata"] \
                    or "reg" in name:
                    detail = map(int, detail)
                else:
                    detail = [int(re.sub(r'\W+', '', d), 16) for d in detail]

                value = detail[0]
                mask = detail[1] if len(detail) > 1 else 0           

                # For metadata assume bottom 32-bits for now.
                if name == "metadata":
                    assert mask == 18446744073709551615
                    mask >>= 32
                match_fields[name] = value

                if name not in ["wildcards", "priority"]:
                    match_fields["%s_mask" % name] = mask

                debug("value=%s, mask=%s" % (hex(value), 
                                             hex(mask) if mask else ""))

            match_fields = self.clear_non_matches(match_fields)
            match_fields.pop("wildcards")

            # Populate with rule filters.
            #flags = {}
            #reg = [None] * self.num_regs
            #reg_masks = [DEFAULT_REG_MASK] * self.num_regs

            # Initialize to all X's (0x3)
            #match_str = wildcard_create_bit_repeat(self.hs_format["length"],\
            #                                       0x3)
            #match_str.priority = match_fields["priority"]

            hs_match = {}
            priority = match_fields["priority"]

            match_fields.pop("priority")

            for name, value in match_fields.items():
                
                # Skip mask fields (set through wildcard including value)
                if "mask" in name:
                    continue

                if "%s_mask" % name in match_fields:
                    mask = match_fields['%s_mask' % name]
                else:
                    mask = 0

                if name == "tunnel_id": 
                    # Split into two fields, otherwise there's a long overflow

                    # tunnel_bh, aka "Bottom half" is TUNNEL[0:32]
                    tunnel_bh = value % (1 << 32)
                    tunnel_bh_mask = mask % (1 << 32)

                    # tunnel_th is TUNNEL[32:63]
                    tunnel_th = value >> 32
                    tunnel_th_mask = mask >> 32
                    #debug("mask_bits = %d" % mask_bits)
                    debug("tunnelID = %s, mask=%s" % \
                            (hex(value), hex(mask)))
                    #debug("tunnel_bh = %s (%d bits)" % \
                    #        (hex(tunnel_bh), tunnel_bh))
                    #debug("tunnel_th = %s (%d bits)" % \
                    #        (hex(tunnel_bh), tunnel_th))

                    hs_match["tunnel_bh"] = \
                        self.tf.get_wc("tunnel_bh", tunnel_bh, tunnel_bh_mask)
                    hs_match["tunnel_th"] = \
                        self.tf.get_wc("tunnel_th", tunnel_th, tunnel_th_mask)

                    #self.tf.set_field(match_str, "tunnel_bh", \
                    #                  tunnel_bh, tunnel_bh_mask)
                    #self.tf.set_field(match_str, "tunnel_th", \
                    #                  tunnel_th, tunnel_th_mask)

                else:
                    hs_match[name] = self.tf.get_wc(name, value, mask) 
                    #self.tf.set_field(match_str, name, value, mask)
                # end for loop - match

            # Continue to rule's actions
            re_action = re.compile(".*\nactions: \[(.*)\].*", re.DOTALL)
            action_text = re_action.match(rule_text).group(1)
            action_groups = action_text.strip().split("message_type: 22")[:-1]

            rule_actions = []
            for a_idx, atext in enumerate(action_groups): 
                re_atype = re.compile(".*type: (?P<type>[^,]*),.*Extensions: \{"
                                      "\n(?P<extensions>.*)\}.*", re.DOTALL)
                m = re_atype.match(atext)
                atype = m.group('type')
                aext = [a.strip() for a in m.group('extensions') \
                                            .split(":", 1)[1] \
                                            .strip().split(",\n")]
                debug("atype => %s, aext => %s" % (atype, aext))

                if atype == "BUNDLE_LOAD":
                    fields = {}
                    for action_field in aext:
                        field, value = action_field.split(": ")
                        fields[field] = value

                    debug("BUNDLE_LOAD fields: %s" % fields)
                    new_ofports = eval(fields["ofports"])
                    assert type(new_ofports) == list

                    new_reg_idx = int(fields["reg_idx"])
                    #new_algorithm = int(fields["algorithm"])
                
                    # Assume using first output port during bundle load
                    new_value = new_ofports[0]
                    field = "reg%d" % (new_reg_idx+1)
                    start = 0
                    stop = 32
                    debug("Adding BUNDLE_LOAD action: %s[%d:%d] <- %d" % \
                            (field, start, stop, new_value))
                    action = self.tf.create_set_action(field, new_value, \
                                                       start, stop)

                    rule_actions.append(action)

                elif atype == "LEARN":
                    action = {} 
                    action["type"] = "learn"
                    action["idx"] = (idx, a_idx)
                    action["string"] = "%s %s" % (atype, aext)

                    rule_actions.append(action)

                elif atype == "NOTE":
                    note_str = aext[0].split(': ',1)[1]
                    pass

                elif atype == "REGISTER_LOAD":
                    output_str, value_str = aext[0].split(': ')[1] \
                                                   .split(' <- ')
                    (dst_field, dst_start, dst_stop) = \
                                self.parse_var(output_str)
      
                    new_value = int(value_str)
                      
                    new_actions = []
                    if dst_field == "tunnel":
                        if dst_stop <= 32:
                            # First half of tunnel ID
                            new_actions.append(self.tf.create_set_action(\
                                "tunnel_bh", new_value, start=dst_start, \
                                 stop=dst_stop))
                        else:
                            # Second half of tunnel ID
                            
                            if dst_start < 32:
                                # If input includes first part, e.g. tunnel[20:57], 
                                # then add actions for both tunnel_bh[20:32] and 
                                # tunnel_bh[0:25]
                                new_actions.append(\
                                    self.tf.create_set_action("tunnel_bh",\
                                            new_value >> (dst_stop - 32),\
                                            dst_start, 32))
                                new_actions.append(\
                                    self.tf.create_set_action("tunnel_th",\
                                           new_value % (1 << (dst_stop - 32)),\
                                           0, dst_stop - 32))
                            else:
                                new_actions.append(\
                                    self.tf.create_set_action(\
                                        "tunnel_th", new_value,\
                                        dst_start - 32, dst_stop - 32))
                    else:
                        new_actions.append(\
                            self.tf.create_set_action(dst_field, new_value,\
                                              dst_start, dst_stop))
                    rule_actions.extend(new_actions)

                elif atype == "REGISTER_MOVE":
                    output_str, value_str = aext[0].split(': ')[1].split(' <- ')
                    (dst_field, dst_start, dst_stop) = self.parse_var(output_str)
                    (src_field, src_start, src_stop) = self.parse_var(value_str)
                      
                    assert dst_stop - dst_start == src_stop - src_start, \
                            Exception("Invalid shift length %s != length %s" % 
                                       (output_str, value_str))
      
                    #debug("before src_field=%s, dst_field=%s" % (src_field, dst_field))
                    if "tunnel" == dst_field:
                        (dst_start, dst_stop, dst_field) = \
                           normalize_tunnel(dst_start, dst_stop, dst_field)

                    if "tunnel" == src_field:
                        (src_start, src_stop, src_field) = \
                           normalize_tunnel(src_start, src_stop, src_field)

                    action = {}
                    action["type"] = "tunnel"
                    action["string"] = "%s <= %s" % (output_str, value_str)
                    action["src_field"] = src_field
                    action["src_start"] = src_start
                    action["src_stop"] = src_stop

                    action["dst_field"] = dst_field
                    action["dst_start"] = dst_start
                    action["dst_stop"] = dst_stop

                    rule_actions.append(action)

                elif atype == "STRIP_VLAN":
                    action = self.tf.create_set_action("dl_vlan_vid", 0)

                    rule_actions.append(action)

                elif atype[:3] == "SET":
                    field = atype[4:].lower()
                    if "vlan_vid" in field:
                        field = "dl_vlan_vid"

                    new_value = aext[-1].split(": ")[1]
                    if ":" in new_value:
                        new_value = int(re.sub(r'\W+', '', new_value), 16)
                    else:
                        new_value = int(new_value)
                    debug("field = %s, new_value = %s", field, new_value)
                    
                    action = self.tf.create_set_action(field, new_value)

                    rule_actions.append(action)

                elif atype == "RESUBMIT":
                    # Re-input to current table
                    new_in_port = int(aext[0].split(" ")[2])

                    action = {} 
                    action["type"] = "fwd"
                    action["table"] = self.table_no
                    action["port"] = new_in_port

                    # If more rules follow, besides NOTE, keep track of re-entry point
                    if a_idx < len(action_groups) - 2:
                        action["idx"] = (idx, a_idx+1)

                    # Add a forward action (set in port header field to port)
                    fwd_action = {}
                    #mask = wildcard_create_bit_repeat(self.hs_format["length"],0x2)
                    #rw = wildcard_create_bit_repeat(self.hs_format["length"],0x1)
                    #self.tf.set_field(mask, "in_port", 0, 0)
                    #self.tf.set_field(rw, "in_port", new_in_port, 0)
                    #fwd_action["mask"] = mask
                    #fwd_action["rw"] = rw

                    fwd_action["field"] = "in_port"
                    fwd_action["start"] = 0
                    fwd_action["value"] = 0
                    fwd_action["string"] = "RESUBMIT: %d" % new_in_port
                    action["fwd_action"] = fwd_action

                    rule_actions.append(action)
                    break

                elif atype == "RESUBMIT_TABLE":
                    new_table = int(aext[0].split(": ", 1)[1])
                    new_port = int(aext[3].split(": ", 1)[1])

                    action = {} 
                    action["type"] = "fwd"

                    # Re-input to current table
                    action["table"] = new_table
                    action["port"] = new_port

                    # If more rules follow, keep track of re-entry point
                    if a_idx < len(action_groups) - 2:
                        action["idx"] = (idx, a_idx)
                    
                    # Add a forward action (set in port header field to port)
                    fwd_action = {}
                    #mask = wildcard_create_bit_repeat(\
                    #        self.hs_format["length"], 0x2)
                    #rw = wildcard_create_bit_repeat(self.hs_format["length"],\
                    #                                0x1)
                    #self.tf.set_field(mask, "in_port", 0, 0)
                    #self.tf.set_field(rw, "in_port", new_port, 0)
                    #debug("Adding FWD action: %s" % \
                    #        (self.tf.wc_to_parsed_string(rw)))

                    fwd_action["field"] = "in_port"
                    fwd_action["start"] = 0
                    fwd_action["value"] = 0
                    #fwd_action["mask"] = mask
                    #fwd_action["rw"] = rw
                    fwd_action["string"] = "RESUBMIT_TABLE: %d/%d" % \
                                            (new_table, new_port)
                    action["fwd_action"] = fwd_action

                    rule_actions.append(action)
                    break

                elif atype == "OUTPUT":
                    port = int(aext[0].split(": ", 1)[1])

                    # Add forward rule
                    action = {} 
                    action["type"] = "output"
                    action["table"] = tf.OUTPUT_PHYSICAL
                    action["port"] = port

                    rule_actions.append(action)

                elif atype in ["OUTPUT_CONTROLLER", "DECREMENT_TTL_CONTROLLER", \
                               "DECREMENT_TTL"]: 

                    action["type"] = "controller"
                    action["idx"] = (idx, a_idx)

                    rule_actions.append(action)

                elif atype == "OUTPUT_REG":
                    value_str = aext[0].split(": ")[1].split(",")[0] \
                                       .split(" = ")[1]

                    # Add drop rule
                    action = {} 
                    action["type"] = "output_reg"
                    (field, start, stop) = self.parse_var(value_str)
                    action["output_reg"] = field
                    action["reg_start"] = start
                    action["reg_length"] = stop - start

                    rule_actions.append(action)

                elif atype in ["STACK_PUSH", "STACK_POP"]:
                    value_str = aext[0].split(': ')[1]
                    debug("Adding %s: %s" % (atype, value_str))

                    (field, start, stop) = self.parse_var(value_str)

                    if "tunnel" == field:
                        (start, stop, field) = \
                            normalize_tunnel(start, stop, field)

                    action = {}
                    action["type"] = atype.lower()
                    action["string"] = "%s: %s" % (atype, value_str)
                    action["field"] = field
                    action["start"] = start
                    action["stop"] = stop

                    rule_actions.append(action)

                else: 
                    raise Exception("Unknown type: %s" % atype)
                a_idx += 1

            # end for loop - actions
            re_id = re.compile("id: (?P<id>\d*)", re.DOTALL)
            rule_id = int(re_id.search(rule_text).group('id'))
            debug("Read Rule idx=%d id=%d, actions: \n%s" % \
                    (idx, rule_id % 10000, \
                     "\n".join([repr(r) for r in rule_actions])))

            has_output = False
            for a in rule_actions:
                if a["type"] in ["fwd", "output", "learn", "output_reg"]:
                    has_output = True
                    break
            
            # If not outputting, add "drop" action 
            if not has_output:
                debug("Adding drop rule")
                action = {} 
                action["type"] = "drop"
                rule_actions.append(action)

            # Treat Table as the port in the transfer function, and use
            # "in port" as a header field
            #def_rule = tf.TF.create_standard_rule([self.table_no], \
            #                    wildcard_to_str(match_str), [], \
            #                    None, None, filename, \
            #                    register_actions=rule_actions)
            self.tf.create_and_add_register_rule(hs_match, rule_actions, 
                             rule_id=rule_id, priority=priority, idx=idx)

            debug("#")
            # end loop rules

        debug("Read %d rules" % (len(self.tf.rules)))
        self.consolidate_hs(results_dir=results_dir)
        debug("Loaded rules: %s", self.dump_rules())
        self.save_to_file()


    def read_file(self, filename):
        '''
          Read in file and create flow table.
        '''

        # debug(header (4 lines))
        f = open(filename)
        chassis = f.read().split("Chassis")[DEFAULT_CHASSIS_NUM]
        self.parse_table(chassis[3:].split("----"), filename=filename)

    def consolidate_actions(self, actions):
        # TODO
        return actions

    def match(self, pkt_in):

        info("Looking for match fe=%s, table_id=%s for %d rules " \
             "for pkt=%s", self.engine_id, self.table_id, \
                len(self.hs_to_output), \
                self.tf.wc_to_parsed_string(pkt_in,skip=['x'*8]))

        action_list = []
        for hs_in in self.hs_to_output:

            if wildcard_is_subset(pkt_in, hs_in.match):
                debug("Match! with hs=%s \n"
                      "with %d applied actions" % \
                         (self.tf.hsmatch_to_str(hs_in, skip=['x'*8]), \
                         len(hs_in.applied_actions)))
                # Match!
                info("Packet header match! with %d actions" % \
                        len(hs_in.applied_actions))
                action_list = hs_in.applied_actions

                # Only allow one match at a time (for now)
                break

        # No match found
        info("Returning %d output actions", len(action_list))
        return action_list

    def consolidate_hs(self, start_table=DEFAULT_TABLE_NO, \
                       results_dir=DEFAULT_RESULTS_DIR):

        # Apply a headerspace object to TF we expect this to match on rule 1,2,4
        #hspace_input = headerspace(self.hs_format["length"])
        #wc = wildcard_create_bit_repeat(self.hs_format["length"],0x3)

        # Results is a list of tuples (headerspace, table), and # of hops
        new_results = self.tf.T(DEFAULT_TABLE_NO, quick_mode=True)
        debug("Received %d new results" % len(new_results))
        self.hs_to_output = new_results

        debug("Result is:\n---------")
        debug("hs_to_output: entries %d", len(self.hs_to_output))
        debug("Optimizations: \n %d subset rules \n %d non-last rules", 
                self.tf.num_subset_rules, self.tf.num_nonlast_rules)

        if results_dir:
            fname = "%s/%s.txt" % (results_dir, self.TEST_FE)
            if not os.path.exists(results_dir):
                os.mkdir(results_dir)

            f = open(fname, 'w')
            debug("Writing results to: %s", fname)
            f.write("FE, Rules, Flows, Subset_Rules_Excluded, Num_Comparisons\n")
            f.write("%s, %s, %s, %s, %s\n" % \
                        (self.TEST_FE, len(self.tf.rules), len(self.hs_to_output), \
                         self.tf.num_subset_rules, self.tf.num_comps))

    def save_to_file(self, filename=None):
    
        if not filename:
            filename = self.default_filename

        info("Saving TF objects to file %s" % filename)

        # TODO: See if we do this in JSON format
        with open(filename, 'wb') as fp:
            pickle.dump(self.hs_to_output, fp)
        #    pickle.dump(self.tf, fp)

    def load_from_file(self, filename=None):

        if not filename:
            filename = self.default_filename

        info("=== Loading TF objects from file %s === " % filename)
        with open(filename, 'rb') as fp:
            self.hs_to_output = pickle.load(fp)
        #    self.tf = pickle.load(fp)

        info("Loaded rules: %s" % self.dump_rules())

    def dump_rules(self):
        strings = []
        for i, hs_in in enumerate(self.hs_to_output):
            strings.append("\nRule %d of %d: hs_in: %s \n" %  \
                              (i+1, len(self.hs_to_output), \
                              hs_in))
        return "\n".join(strings)

def main():
  
    global verbose, flow_file 

    #import datetime
    #logname = "python_%s.log" % datetime.datetime.now().strftime("%m%d_%H%M.%S")
    logname = "python.log"
    basicConfig(filename=logname, level=DEBUG)
    print "Logging to %s" % logname

    from optparse import OptionParser
    parser = OptionParser()
    #parser.add_option("-f", "--file", dest="filename", \
    #                  default=DEFAULT_FLOW_FILE,
    #                  help="flow file to evaluate", metavar="FILE")
    parser.add_option("-r", "--redo", action="store_true", dest="redo", \
                      default=False, \
                      help="redo flow parsing, even if file exists")
    parser.add_option("-d", "--results_dir", default=DEFAULT_RESULTS_DIR, \
                      dest="results_dir", help="directory to store results")
    parser.add_option("-q", "--quiet",
                      action="store_false", dest="verbose", default=True,
                      help="don't print status messages to stdout")
    parser.add_option("-m", "--min_rules", type="int",
                      dest="min_rules", default=DEFAULT_MIN_RULES,
                      help="min number of rules")
    parser.add_option("-x", "--max_rules", type="int",
                      dest="max_rules", default=DEFAULT_MAX_RULES,
                      help="max number of rules")
    parser.add_option("-c", "--hop_cutoff", type="int",
                      default=DEFAULT_HOP_CUTOFF,
                      help="cutoff before stopping routing")

    (options, args) = parser.parse_args()

    import re
    #for full_path in options.filename.split(','):
    print "Results directory: %s" % options.results_dir
    print "%d file(s)" % len(args)
    for full_path in args:
        print "Processing %s...." % full_path,

        f = open(full_path, 'r')
        rule_text = f.read()

        p = re.compile('.*?\nid: [0-9]+', re.S)
        rules = p.findall(rule_text)

        from os import path
        fe_id = path.basename(full_path).split('_')[1].split('.')[0]
        topo_id = path.dirname(full_path).split('topo-')[1].split('.')[0]
        fname = "%s_%s" % (topo_id, fe_id)
        classifier = FlowClassifier(TEST_FE=fname, \
                                    hop_cutoff=options.hop_cutoff)
        classifier.parse_rules(rules, filename=fname, redo=options.redo, \
                               results_dir=options.results_dir, \
                               min_rules=options.min_rules, \
                               max_rules=options.max_rules)
        print "done"
        del classifier

#  # Computing Inverse Transfer function
#  debug("\n--------\nINVERSE\n--------\n")
#  result = classifier.tf.T_inv(h, 5)
#  debug("Result is:\n---------")
#  for (h,p) in result:
#    debug("at port %s:\n%s"%(p,h))
#    debug("#")

  # Output should be a list of tuples of (input space, action)
  #space_to_action = classifier.slice_spaces(initial_space)
  
if __name__ == "__main__":
  main()
